<?php

return array(
    "index" => array("method" => "index"),
    "getAllActiveUsers" => array("method" => "getAllActiveUsers"),
    "getUserInformation" => array("method" => "getUserBaseInformation"),
    "updateUsersInfo" => array("method" => "updateUsersInfo", "controller" => "updateController"),
    "prepareDataToUpdate" => array("method" => "prepareUpdate", "controller" => "updateController"),
    "fileImport" => array("method" => "createImport", "controller" => "ImportController"),
    "executeImport" => array("method" => "executeImport", "controller" => "ImportController"),
    "downloadFile" => array("method" => "getImportFileFromFTP", "controller" => "FtpController"),
    "pushCsv" => array("method" => "pushImportFileToFTP", "controller" => "FtpController"),
    "downloadUpdateFile" => array("method" => "getUpdateFileFromFTP", "controller" => "FtpController"),
    "fixSource" => array("method" => "fixUsers", "controller" => "FixController"),
    "fixUser" => array("method" => "fixUser", "controller" => "FixController"),
);