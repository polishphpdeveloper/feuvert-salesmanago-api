<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ActionController
 *
 * @author Marek S
 */
class ActionController extends Controller
{
    public function index()
    {
        $json = array();
        $data = array();
        foreach ($this->newsletter->getFields() as $field) {
            $value = '';
            if (isset($_REQUEST[$field])) {
                $value = urldecode($_REQUEST[$field]);
            }
            $data[$field] = $value;
        }
        $response = $this->newsletter->add($data);
        $json['validate'] = $response->success;
        $json['response'] = $response;
        $json['message'] = $this->newsletter->getValidationMessage();
        echo json_encode($json);
    }
    
    public function getAllActiveUsers()
    {
        set_time_limit(10000);
        $json = array();
        $json['validation'] = true;
        $iterator = 0;
        $obj = false;
        do {
            $list = NewsletterList::getAllUsers($this->communication, $iterator);
            if (count($list) <= 0) {
                break;
            }
            $csvData = array();
            foreach ($list as $newsletter) {
                if (!$newsletter->optedOut) {
                    $csvData[] = $newsletter->prepareDataToCsv();
                }
            }
            if (!$obj) {
                $obj = new CSVWriter(Config::getInstance()->fileToImport, array(), true);
                $obj->reopenFile();
            } else {
                $obj->addContent($csvData);
            }
            $iterator++;
            unset($csvData);
        } while ($iterator <= 20);

        echo json_encode($json);
    }

    public function getUserBaseInformation()
    {
        $json = array();
        $json['validation'] = false;
        $json['message'] = 'You have to set email.';
        if (isset($_REQUEST['email'])) {
            $email = $_REQUEST['email'];
            $response = NewsletterList::getUserByEmail($this->communication, $email);
            if (is_object($response) || is_array($response) && isset($response->email)) {
                $json['validation'] = true;
                $json['response'] = $response->getJsonData();
                $json['date'] = $this->prepareDateToFix($response);
                unset($json['message']);
            }
        }
        echo json_encode($json);
    }
}
