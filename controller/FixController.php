<?php

/**
 * Created by PhpStorm.
 * User: Programista
 * Date: 2016-04-11
 * Time: 09:56
 */
class FixController extends Controller
{
    public function fixUser()
    {
        $json = array();
        $json['validation'] = false;
        $json['message'] = 'You have to set email.';
        if (isset($_REQUEST['email'])) {
            $email = $_REQUEST['email'];
            $newsletter = NewsletterList::getUserByEmail($this->communication, $email);
            $json['message'] = 'Object not found.';
            if (is_object($newsletter) || is_array($newsletter) && isset($newsletter->email)) {
                if ($newsletter->findInProperty('Data') == "") {
                    $newsletter->createdOn = $newsletter->createdOn / 1000;
                    $properties = array(
                        "source" => "Serwis",
                        "date" => date('Y-m-d H:i', $newsletter->createdOn)
                    );
                    $date = $this->prepareDateToFix($newsletter);
                    if ($date) {
                        $properties = array(
                            "source" => $date[0],
                            "date" => $date[1]
                        );
                    }
                    $newsletter->source = $properties['source'];
                    $newsletter->date = $properties['date'];
                    $resp = $newsletter->add();
                    $array = array("response" => json_encode($resp), "email" => $newsletter->email);
                    $json['response'] = $array;
                    $json['response'] = $array;
                }

            }
        }
        echo json_encode($json);
    }

    public function fixUsers()
    {
        set_time_limit(10000);
        $data = array();
        for ($iterator = 0; $iterator < 10; $iterator++) {
            $response = NewsletterList::getAllUsers($this->communication, $iterator);
            foreach ($response as $newsletter) {
                if ($newsletter->findInProperty('Data') == "") {
                    $newsletter->createdOn = $newsletter->createdOn / 1000;
                    $properties = array(
                        "source" => "Serwis",
                        "date" => date('Y-m-d H:i', $newsletter->createdOn)
                    );
                    $date = $this->prepareDateToFix($newsletter);
                    if ($date) {
                        $properties = array(
                            "source" => $date[0],
                            "date" => $date[1]
                        );
                    }
                    $newsletter->source = $properties['source'];
                    $newsletter->date = $properties['date'];
                    $resp = $newsletter->add();
                    $array = array("response" => json_encode($resp), "email" => $newsletter->email);
                    $data[] = $array;
                }

            }
        }
        echo json_encode($data);
    }
}