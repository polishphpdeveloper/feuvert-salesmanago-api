<?php
/**
 * Created by PhpStorm.
 * User: Programista
 * Date: 2016-04-11
 * Time: 09:49
 */

class FtpController extends Controller
{
    public function getImportFileFromFTP()
    {
        $ftpConfig = Config::getInstance()->ftp;
        $json = array();
        $json['validation'] = true;
        $json['message'] = 'Cannot established connection to ftp ' . $ftpConfig->url . '.';
        $ftp = ftp_connect($ftpConfig->url);
        if (ftp_login($ftp, $ftpConfig->user, $ftpConfig->password)) {
            ftp_chdir($ftp, $ftpConfig->dir);
            if (ftp_get($ftp, $ftpConfig->fileName, Config::getInstance()->importFile, FTP_BINARY)) {
                $json['message'] = 'Success';
            } else {
                $json['message'] = 'Cannot download file ' . $ftpConfig->fileName . ' from ftp ' . $ftpConfig->url . ' .';
            }
        }
        echo json_encode($json);
    }

    public function pushImportFileToFTP()
    {
        $ftpConfig = Config::getInstance()->ftp;
        $json = array();
        $json['validation'] = true;
        $json['message'] = 'Cannot established connection to ftp ' . $ftpConfig->url . '.';
        $ftp = ftp_connect($ftpConfig->url);
        if (ftp_login($ftp, $ftpConfig->user, $ftpConfig->password)) {
            ftp_chdir($ftp, $ftpConfig->dir);
            if (ftp_put($ftp, Config::getInstance()->fileToImport, Config::getInstance()->fileToImport, FTP_ASCII)) {
                $json['message'] = 'Success';
            } else {
                $json['message'] = 'Cannot push file to ftp.';
            }
        }
        echo json_encode($json);
    }

    public function getUpdateFileFromFTP()
    {
        $ftpConfig = Config::getInstance()->ftp;
        $json = array();
        $json['validation'] = true;
        $json['message'] = 'Cannot established connection to ftp ' . $ftpConfig->url . '.';
        $ftp = ftp_connect($ftpConfig->url);
        if (ftp_login($ftp, $ftpConfig->user, $ftpConfig->password)) {
            ftp_chdir($ftp, $ftpConfig->dir);
            if (ftp_get($ftp, Config::getInstance()->updateFile, Config::getInstance()->updateFile, FTP_BINARY)) {
                $json['message'] = 'Success';
            } else {
                $json['message'] = 'Cannot download file ' . Config::getInstance()->updateFile . ' from ftp ' . $ftpConfig->url . ' .';
            }
        }
        echo json_encode($json);
    }
}