<?php

/**
 * Created by PhpStorm.
 * User: Programista
 * Date: 2016-04-11
 * Time: 10:00
 */
class ImportController extends Controller
{
    public function createImport()
    {
        set_time_limit(10000);
        $file = Config::getInstance()->importFile;
        $json = array(
            'validation' => file_exists($file),
            'addedRecords' => array(),
            'notAddedRecords' => array()
        );
        if (!$json['validation']) {
            $json['message'] = 'Import file doesn\'t exists.';
        } else {
            $csv = new CSVReader($file);
            foreach ($csv->getData() as $array) {
                $data = $this->prepareNewsletterFieldByArray($array);
                $data['source'] = 'serwis';
                $newLogger = array(
                    'email' => $data['email'],
                    'checked' => 0,
                    'data' => json_encode($data),
                );
                try {
                    $logger = Logger::create($newLogger);
                    $logger->save();
                    $json['addedRecords'][] = $newLogger;
                } catch (Exception $exception) {

                }
            }
        }
        echo json_encode($json);
    }

    public function executeImport()
    {
        $loggers = Logger::all(array('conditions' => 'checked <= 0', 'limit' => Config::getInstance()->updateExecutionObjectsLimit));
        $import = new ImportFromLogger($loggers, $this->newsletter);
        $json = array(
            'validation' => true,
            'fields' => $import->getFields(),
            'count' => count($import)
        );
        echo json_encode($json);
    }
}