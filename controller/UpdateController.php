<?php

/**
 * Created by PhpStorm.
 * User: Programista
 * Date: 2016-04-11
 * Time: 10:16
 */
class UpdateController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        set_time_limit(1000000);
    }

    public function prepareUpdate()
    {
        $json = array("inserted" => array(), "updated" => array());
        $parser = new UpdateCsvParser($this->communication);
        while ($obj = $parser->next()) {
            $updateLogger = UpdateLogger::first(array('email' => $obj->email));
            $data = json_encode($obj->getData());
            if (!$updateLogger) {
                $data = array(
                    'checked' => 0,
                    'data' => json_encode($obj->getData()),
                    'email' => $obj->email,
                    'response_status' => 'Empty'
                );
                UpdateLogger::create($data);
                $json['inserted'] = $obj->email;
            }
            else{
                if($data != $updateLogger->data){
                    $updateLogger->data = $data;
                    $updateLogger->checked = 0;
                    $updateLogger->save();
                    $json['updated'] = $obj->email;
                }
            }
        }
        echo json_encode($json);
    }

    public function updateUsersInfo()
    {
        $loggers = UpdateLogger::all(array('conditions' => 'checked <= 0', 'limit' => Config::getInstance()->importExecutionObjectsLimit));
        $import = new ImportFromLogger($loggers, $this->newsletter);
        $json = array(
            'validation' => true,
            'fields' => $import->getFields(),
            'count' => count($import)
        );
        echo json_encode($json);
    }
}