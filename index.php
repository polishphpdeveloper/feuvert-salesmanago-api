<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
define('APP_PATH', __DIR__);

spl_autoload_register(function ($name) {
    $dirs = array('model', 'vendor', 'controller', 'model/activerecord');
    foreach ($dirs as $dir){
        $file =  APP_PATH . '/' . $dir . '/' . $name . '.php';
        if (file_exists($file)){
            include_once $file;
            return true;
        }
    }
});
$core = AppCore::getInstance();
$core->callCurrentActionFromPost();
