<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Newsletter
 *
 * @author Marek S
 */
class Newsletter extends Model
{

    protected $options;

    protected $fields = array(
        "email",
        "carnumber",
        "serwis",
        "name",
        "source_ecommerce",
        "source"
    );

    protected $propertiesName = array(
        "serwis" => "Serwis",
        "carnumber" => "Numer rejestracyjny"
    );


    private function prepareProperties()
    {
        $properties = array();
        foreach ($this->propertiesName as $key => $field) {
            if (isset($this->data[$key])) {
                $properties[$field] = $this->data[$key];
            }
        }
        if(isset($this->data['properties']) && is_array($this->data['properties'])) {
            foreach ($this->data['properties'] as $key => $field) {
                if (!is_string($field)) {
                    continue;
                }
                if ($field == '') {
                    $field = '-';
                }
                $properties[$key] = $field;
            }
        }
        if (isset($this->data['source'])) {
            $properties['Źródło'] = $this->data['source'];
            unset($this->data['source']);
        } elseif (isset($this->data['source_ecommerce']) && $this->data['source_ecommerce'] != "") {
            $properties['Źródło'] = 'Ecommerce';
        } elseif (!isset($this->data['source']) && !isset($this->data['source_ecommerce'])) {
            $properties['Źródło'] = 'Newsletter';
        } else {
            $properties['Źródło'] = 'Serwis';
        }
        if (isset($this->data['date'])) {
            $properties['Data'] = $this->data['date'];
        } else {
            $properties['Data'] = date('Y-m-d H:i');
        }
        return $properties;
    }

    private function prepareOptions()
    {
        if ($this->communication->hasContact($this->data['email'])->result) {
            $this->options['useApiDoubleOptIn'] = false;
        }
        if (isset($this->data['source_ecommerce']) && $this->data['source_ecommerce'] != "") {
            $this->options['useApiDoubleOptIn'] = false;
            $this->options['forceOptIn'] = true;
        }
    }

    private function prepareDataToUpsert()
    {
        $data = array(
            $this->fields[0] => $this->data[$this->fields[0]],
            $this->fields[3] => $this->data[$this->fields[3]],
            'company' => '',
            'phone' => '',
            'state' => 'CUSTOMER'
        );
        return $data;
    }

    protected function validate()
    {
        if ($this->checkIsNotEmpty('email')) {
            if ($this->isEmail('email')) {
                $this->validationMessage = 'Record added.';
                return true;
            } else {
                $this->validationMessage = 'Field email should be email.';
            }
        } else {
            $this->validationMessage = 'Email field is required.';
        }
        return false;
    }

    public function __construct($communication)
    {
        $this->options = array(
            'lang' => 'PL',
            'useApiDoubleOptIn' => true,
            'forceOptIn' => false,
            'forceOptOut' => false,
            'forcePhoneOptIn' => false,
            'forcePhoneOptOut' => false,
        );
        parent::__construct($communication);
    }

    public function addProperties($name, $value)
    {
        $this->data['properties'][$name] = $value;
    }

    public function findInProperty($name)
    {
        if ($this->isPropertyExists("properties")) {
            $properties = $this->data['properties'];
            foreach ($properties as $property) {
                if(!is_object($property) && !is_array($property)){
                    continue;
                }
                if ($property->name == $name) {
                    return $property->value;
                }
            }
        }
        return "";
    }

    public function add($data = false)
    {
        if ($data) {
            $this->data = $data;
        }
        if ($this->validate()) {
            $properties = $this->prepareProperties();
            $data = $this->prepareDataToUpsert();
            $this->prepareOptions();
            return $this->communication->addNewsletterUser($data, $properties, $this->options);
        }
        return false;
    }

    public function prepareDataToCsv()
    {
        $serwis = "";
        $carnumber = "";
        if ($this->isPropertyExists("properties")) {
            $serwis = strlen($this->findInProperty("Serwis")) > 0 ? $this->findInProperty("Serwis") : $this->findInProperty("numer serwisu");
            $carnumber = strlen($this->findInProperty("Numer rejestracyjny")) > 0 ? $this->findInProperty("Numer rejestracyjny") : $this->findInProperty("nr rejestracyjny");
        }
        $data = array(
            $this->isPropertyExists("email") ? $this->email : "",
            $carnumber,
            $serwis,
            $this->isPropertyExists("name") ? $this->name : "",
        );
        return $data;
    }

}
