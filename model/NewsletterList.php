<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author webmaster
 */
class NewsletterList extends ModelList {

    protected function __construct($communication, $data) {
        parent::__construct($communication, $data, 'Newsletter');
    }

    public static function getAllUsers($communication, $page) {
        $data = $communication->getPagedList($page, 1000)->contacts;
        if(count($data) <= 0){
            return array();
        }
        $obj = new NewsletterList($communication, $data);
        return $obj;
    }

    public static function getUsersByEmail($communication, $email){
        $data = $communication->getBaseInformation($email);
        if(count($data) <= 0){
            return array();
        }
        $obj = new NewsletterList($communication, $data->contacts);
        return $obj;
    }

    public static function getUserByEmail($communication, $email){
        $data = self::getUsersByEmail($communication, $email);
        if(is_array($data->data)){
            return array_pop($data->data);
        }
        return new Newsletter($communication);
    }
}
