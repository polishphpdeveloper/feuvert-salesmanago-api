<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AppCore
 *
 * @author Marek S
 */
$filename = APP_PATH . '/vendor/activerecord/ActiveRecord.php';
if (!file_exists($filename)) {
    throw new Exception('Active record file doesn\'t found.');
}
include_once $filename;

class AppCore {

    private $router;
    private $dbConnection;

    private function attachActiveRecord() {
        $config = Config::getInstance()->database;
        $string = 'mysql://' . $config->user . ':' . $config->password . '@' . $config->host . '/' . $config->name . '';
        ActiveRecord\Config::initialize(function($cfg) use ($string) {
            $cfg->set_model_directory('model/activerecord');
            $cfg->set_connections(array(
                'development' => $string));
        });
    }

    private function __construct() {
        $data = include_once(APP_PATH . '/config/router.php');
        $this->router = new Router($data);
        $this->attachActiveRecord();
    }

    public static function getInstance() {
        static $instance;
        if (!isset($instance)) {
            $instance = new self();
        }
        return $instance;
    }

    public function callAction($action) {
        $controller = $this->router->getControllerName($action);
        $method = $this->router->getMethodName($action);
        $object = new $controller();
        if (method_exists($object, $method)) {
            $object->$method();
            return true;
        }
        throw new Exception('Method ' . $method . ' doesn\'t exist in ' . $controller . '.');
    }

    public function callCurrentActionFromPost() {
        $action = '';
        if (isset($_REQUEST['action'])) {
            $action = $_REQUEST['action'];
        }
        $this->callAction($action);
    }

    public function getDbConnection() {
        return $this->dbConnection;
    }

}
