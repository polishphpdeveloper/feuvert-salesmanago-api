<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class CSVReader {

    private $data;
    private $file;
    private $delimiter;

    private function parseFile() {
        if (($handle = fopen($this->file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, $this->delimiter)) !== FALSE) {
                $this->data[] = $data;
            }
            fclose($handle);
        }
    }

    public function __construct($file, $delimiter = ';') {
        $this->data = array();
        $this->file = $file;
        $this->delimiter = $delimiter;
        $this->parseFile();
    }

    public function getData() {
        return $this->data;
    }

}
