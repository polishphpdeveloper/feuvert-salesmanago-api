<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CSVWriter
 *
 * @author webmaster
 */
class CSVWriter {

    private $data;
    private $handle;
    private $file;

    private function write() {
        foreach ($this->data as $array) {
            $string = implode(',', $array) . PHP_EOL;
            fwrite($this->handle, $string);
        }
    }

    public function closeFile() {
        fclose($this->handle);
    }

    public function openFile($new) {
        if (!$new) {
            $this->handle = fopen($this->file, 'a+');
        } else {
            $this->handle = fopen($this->file, 'w+');
        }

        if ($this->handle) {
            return true;
        }
        throw new Exception('Cannot create file.');
    }

    public function reopenFile($new = false) {
        $this->openFile($new);
    }

    public function __construct($file, $data, $new = true) {
        $this->data = $data;
        $this->file = $file;
        $this->openFile($new);
        $this->write();
    }
    
    public function addContent($data){
        $this->data = $data;
        $this->write();
    }

}
