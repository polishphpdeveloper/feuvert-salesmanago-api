<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Communication
 *
 * @author Marek S
 */
class Communication
{

    private $config;
    private $data;
    private $url;

    private function prepareDeafultData()
    {
        $array = array(
            'clientId' => $this->config->clientId,
            'apiKey' => $this->config->apiKey,
            'requestTime' => time(),
            'sha' => sha1($this->config->apiKey . $this->config->clientId . $this->config->apiSecret),
            'owner' => $this->config->owner, //TU PROSZĘ DODAĆ EMAIL WŁAŚCICIELA KONTAKTU 
        );
        $this->data = $array;
    }

    private function sendPostRequest()
    {
        $data = json_encode($this->data);
        $ch = curl_init($this->url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data)
            )
        );
        $response = curl_exec($ch);
        $this->prepareDeafultData();
        if (strlen($response) > 0) {
            return json_decode($response);
        }
        return false;
    }

    public function __construct($config)
    {
        $this->config = $config;
        $this->prepareDeafultData();
    }

    public function addNewsletterUser(array $data, $properties = array(), $options = array())
    {
        foreach ($options as $key => $option) {
            $this->data[$key] = $option;
        }
        $this->data['properties'] = $properties;
        $this->data['contact'] = $data;
        $this->url = 'http://www.salesmanago.pl/api/contact/upsert';
        return $this->sendPostRequest();
    }

    public function addTagsToUser($email, $tags)
    {
        $this->data['email'] = $email;
        if (is_array($tags)) {
            $this->data['tags'] = $tags;
        } else {
            $this->data['tags'] = $tags;
        }
        $this->url = 'http://www.salesmanago.pl/api/contact/managetags';
        return $this->sendPostRequest();
    }

    public function hasContact($email)
    {
        $this->data['email'] = $email;
        $this->url = 'http://www.salesmanago.pl/api/contact/hasContact';
        $response = $this->sendPostRequest();
        return $response;
    }

    public function getPagedList($page, $size)
    {
        $this->data['page'] = $page;
        $this->data['size'] = $size;
        $this->url = 'http://www.salesmanago.pl/api/contact/paginatedContactList';
        $response = $this->sendPostRequest();
        return $response;
    }

    public function getBaseInformation($email)
    {
        if (!is_array($email)) {
            $email = array($email);
        }
        $this->url = 'http://www.salesmanago.pl/api/contact/list ';
        $this->data['email'] = $email;
        $response = $this->sendPostRequest();
        return $response;
    }

}
