<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Config {

    private $data;

    private function __construct($data) {
        $this->data = $data;
    }
    
    public static function getInstance() {
        static $instance;
        if (!isset($instance)) {
            $data = include_once APP_PATH . '/config/config.php';
            $instance = new Config($data);
        }
        return $instance;
    }

    public function __set($name, $value) {
        $this->data[$name] = $value;
    }
    
    public function __get($name) {
        if(!isset($this->data[$name])){
            throw new Exception($name . ' not found on config.');
        }
        if(is_array($this->data[$name])){
            return new self($this->data[$name]);
        }
        return $this->data[$name];
    }
}
