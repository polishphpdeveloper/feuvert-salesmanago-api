<?php

/**
 * Created by PhpStorm.
 * User: Programista
 * Date: 2016-04-11
 * Time: 09:51
 */
abstract class Controller
{
    protected $communication;
    protected $newsletter;

    protected function prepareNewsletterFieldByArray($array)
    {
        $data = array();
        foreach ($this->newsletter->getFields() as $key => $field) {
            if ($key > 3) {
                break;
            }
            $data[$field] = $array[$key];
        }
        return $data;
    }

    protected function prepareDateToFix($newsletter)
    {
        $sourceFields = array(
            "newsletter" => "Źródło zapisu-newsletter",
            "ecommerce" => "Źródło zapisu-ecommerce",
            "serwis" => "Źródło zapisu-serwis",
        );
        foreach ($sourceFields as $name => $field) {
            $value = $newsletter->findInProperty($field);
            if ($value != "") {
                return array($name, $value);
            }
        }
        return false;
    }

    public function __construct()
    {
        $this->communication = new Communication(Config::getInstance());
        $this->newsletter = new Newsletter($this->communication);
    }
}