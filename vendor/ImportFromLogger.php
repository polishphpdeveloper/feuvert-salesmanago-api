<?php
/**
 * Created by PhpStorm.
 * User: Programista
 * Date: 2016-04-11
 * Time: 11:46
 */

class ImportFromLogger implements Countable{
    private $collection;
    private $fields;
    private $newsletter;

    private function execute(){
        foreach ($this->collection as $logger) {
            $data = json_decode($logger->data);
            $status = $this->newsletter->add((array)$data)->success;
            $logger->response_status = 'Błąd podczas komunikacji z API SalesManago';
            if ($status) {
                $logger->checked = 1;
                $logger->response_status = date('Y-m-d H:i');
            }
            $this->fields[] = array(
                "email" => $logger->email,
                "status" => $status,
                "message" => $this->newsletter->getValidationMessage()
            );
            $logger->save();
        }
    }

    public function __construct($collection, $newsletter){
        if(!is_array($collection)){
            throw new Exception('Object is not collection.');
        }
        $this->collection = $collection;
        $this->newsletter = $newsletter;
        $this->fields = array();
        $this->execute();
    }

    public function getFields(){
        return $this->fields;
    }

    public function count(){
        return count($this->fields);
    }
}