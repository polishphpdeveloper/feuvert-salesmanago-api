<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class Model {

    protected $fields;
    protected $communication;
    protected $data;
    protected $validationMessage;
    protected $primaryKey;

    abstract protected function validate();

    protected function checkIsNotEmpty($field) {
        return isset($this->data[$field]) && $this->data[$field] != '';
    }

    protected function prepareEmptyFields() {
        foreach ($this->fields as $field) {
            $this->data[$field] = "";
        }
    }

    protected function isEmail($field) {
        $email = isset($this->data[$field]) ? $this->data[$field] : '';
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    public function __construct($communication) {
        $this->prepareEmptyFields();
        $this->communication = $communication;
        $this->validationMessage = 'Validation success.';
    }

    public function getFields() {
        return $this->fields;
    }

    public function getValidationMessage() {
        return $this->validationMessage;
    }

    public function setData($data) {
        if (is_object($data)) {
            $data = (array) $data;
        }
        $this->data = $data;
    }

    public function __get($obj){
        if(isset($this->data[$obj])){
            $obj = $this->data[$obj];
            if(is_array($obj) && !isset($obj[0]) && !isset($obj[1])){
                $obj = new stdClass($obj);
            }
            return $obj;
        }
        throw new Exception('Cannot find property ' . $obj . '.');
    }
    
    public function isPropertyExists($obj){
        try{
            $this->__get($obj);
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    public function __set($obj, $value){
        $this->data[$obj] = $value;
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function __unset($name)
    {
        unset($this->data[$name]);
    }

    public function getJsonData(){
        return json_encode($this->data);
    }

    public function getData(){
        return $this->data;
    }
}
