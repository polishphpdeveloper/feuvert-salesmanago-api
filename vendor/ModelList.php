<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModelList
 *
 * @author webmaster
 */
class ModelList implements IteratorAggregate {

    protected $data;
    protected $objectName;
    protected $dataToPrepare;
    protected $communication;

    protected function prepareData() {
        foreach ($this->dataToPrepare as $element) {
            $model = new $this->objectName($this->communication);
            $model->setData($element);
            $this->data[] = $model;
        }
    }

    protected function __construct($communication, $data, $objectName) {
        $this->objectName = $objectName;
        $this->communication = $communication;
        $this->dataToPrepare = $data;
        $this->prepareData();
    }

    public function getIterator() {
        return new ArrayIterator($this->data);
    }

}
