<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Router
 *
 * @author Marek S
 */
class Router {

    private $data;
    private $defaultController;

    public function __construct($data) {
        $this->data = $data;
        $this->defaultController = 'ActionController';
    }

    public function getMethodName($action) {
        if (isset($this->data[$action])) {
            return $this->data[$action]['method'];
        }
        return false;
    }

    public function getControllerName($action) {
        if (isset($this->data[$action])) {
            if (isset($this->data[$action]['controller'])) {
                return $this->data[$action]['controller'];
            }
        }
        return $this->defaultController;
    }

}
