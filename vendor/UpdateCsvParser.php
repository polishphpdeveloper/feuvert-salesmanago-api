<?php

/**
 * Created by PhpStorm.
 * User: Programista
 * Date: 2016-03-25
 * Time: 09:47
 */
class UpdateCsvParser
{
    private static $EMAIL_POSITION = 0;
    private static $NAME_POSITION = 3;
    private static $SEX_POSITION = 4;
    private $data;
    private $header;
    private $iterator;
    private $communication;
    private $element;
    private $object;

    private $fields = array(
        "serwis" => 2,
        "carnumber" => 1,
        "name" => 3,
    );

    private $fieldToRetranslate = array(
        "Data" => -1,
        "Źródło" => -2
    );

    private $fieldsToUnset = array(
        "plec" => 4
    );

    private function prepareElement()
    {
        foreach ($this->fields as $key => $field) {
            if($key == "name"){
                if($this->element[$field] == ""){
                    continue;
                }
            }
            if (isset($this->element[$field])) {
                $this->object->{$key} = $this->element[$field];
            }
        }
    }

    private function prepareProperties()
    {
        foreach ($this->header as $key => $element) {
            if ($key == self::$NAME_POSITION && $key == self::$SEX_POSITION) {
                continue;
            }
            $value = '-';
            if (isset($this->element[$key])) {
                $value = $this->element[$key];
            }
            $this->object->addProperties($element, $value);
        }
    }

    private function prepareHeader()
    {
        $this->header = $this->data[$this->iterator];
        $count = count($this->header);
        unset($this->data[$this->iterator]);
        foreach ($this->fields as $field) {
            unset($this->header[$field]);
        }
        foreach ($this->fieldToRetranslate as $translate => $key) {
            if ($key < 0) {
                $this->header[$count + $key] = $translate;
            } else {
                $this->header[$key] = $translate;
            }
        }
        foreach($this->fieldsToUnset as $key => $ufield){
            unset($this->header[$ufield]);
        }
        unset($this->header[self::$EMAIL_POSITION]);
        $this->next();
    }

    private function loadElement()
    {
        $this->object = NewsletterList::getUserByEmail($this->communication, $this->element[self::$EMAIL_POSITION]);
        // var_dump($this->object->getData());die;
        $this->prepareElement();
        $this->prepareProperties();
        return $this->object;
    }

    public function __construct($communication)
    {
        $csv = new CSVReader(Config::getInstance()->updateFile, '|');
        $this->iterator = 0;
        $this->communication = $communication;
        $this->data = $csv->getData();
        $this->prepareHeader();
    }

    public function next()
    {
        $this->iterator++;
        if (isset($this->data[$this->iterator])) {
            $this->element = $this->data[$this->iterator];
            return $this->loadElement();
        }
        return false;
    }
}